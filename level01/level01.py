from string import maketrans,ascii_lowercase  # Required to call maketrans function.


def translate(str):
    """Generate a resulting cipher with elements shown"""
    intab = ascii_lowercase
    outtab = "cdefghijklmnopqrstuvwxyzab"
    trantab = maketrans(intab, outtab)

    print str.translate(trantab)
