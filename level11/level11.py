from PIL import Image,ImageDraw

img = Image.open("cave.jpg")
(w, h) = img.size

even_img = Image.new('RGB',(w // 2, h // 2))
odd_img = Image.new('RGB',(w // 2, h // 2))

for x in range(w):
    for y in range(h):
        p = img.getpixel((x,y))
        if ((x + y) % 2 == 0):
            even_img.putpixel((x // 2, y // 2), p)
        else:
            odd_img.putpixel((x // 2, y // 2), p)

odd_img.show()
even_img.show()
