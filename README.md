# Python Challenge


Repo to store solutions to the [Python Challenge](http://www.pythonchallenge.com)


[Level 00](http://www.pythonchallenge.com/pc/def/0.html)

[Level 01](http://www.pythonchallenge.com/pc/def/map.html)

[Level 02](http://www.pythonchallenge.com/pc/def/ocr.html)

[Level 03](http://www.pythonchallenge.com/pc/def/equality.html)

[Level 04](http://www.pythonchallenge.com/pc/def/linkedlist.php)

[Level 05](http://www.pythonchallenge.com/pc/def/peak.html)

[Level 06](http://www.pythonchallenge.com/pc/def/channel.html)

[Level 07](http://www.pythonchallenge.com/pc/def/oxygen.html)

[Level 08](http://www.pythonchallenge.com/pc/def/integrity.html)

[Level 09](http://www.pythonchallenge.com/pc/return/good.html)

[Level 10](http://www.pythonchallenge.com/pc/return/bull.html)

[Level 11](http://www.pythonchallenge.com/pc/return/5808.html)

[Level 12](http://www.pythonchallenge.com/pc/return/evil.html)

[Level 13](http://www.pythonchallenge.com/pc/return/disproportional.html)

[Level 14](http://www.pythonchallenge.com/pc/return/italy.html)
